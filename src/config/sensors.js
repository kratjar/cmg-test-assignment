'use strict';

export const SENSOR_CLASSIFICATION = {
  thermometer: [
    {
      classification: 'ultra precise',
      condition: ({ realValue, mean, std }) =>
        Math.abs(realValue - mean) <= 0.5 && std < 3.0,
    },
    {
      classification: 'very precise',
      condition: ({ realValue, mean, std }) =>
        Math.abs(realValue - mean) <= 0.5 && std < 5.0,
    },
    { classification: 'precise' },
  ],
  humidity: [
    {
      classification: 'keep',
      condition: ({ realValue, value }) => Math.abs(realValue - value) <= 1,
    },
    { classification: 'discard' },
  ],
  monoxide: [
    {
      classification: 'keep',
      condition: ({ realValue, value }) => Math.abs(realValue - value) <= 3,
    },
    { classification: 'discard' },
  ],
};

export const SENSORS_CLASSIFIED_USING_MEAN_AND_STD = ['thermometer'];

export const SENSORS_CLASSIFIED_USING_VALUE_AND_REAL_VALUE = [
  'humidity',
  'monoxide',
];
