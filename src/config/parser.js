'use strict';

export const PARSER = {
  records: {
    separator: '\r\n',
  },
  reference: {
    separator: ' ',
    referenceIdentifier: 'reference',
    referenceIdentifierIdx: 0,
    sensors: ['thermometer', 'humidity', 'monoxide'],
  },
  sensor: {
    separator: ' ',
    typeIdx: 0, // possible sensor types are defined within reference part of PARSER object
    nameIdx: 1,
  },
  reading: {
    separator: ' ',
    dateTimeIdx: 0,
    valueIdx: 1,
  },
};
