import { SENSOR_CLASSIFICATION } from './sensors';

const { thermometer, humidity, monoxide } = SENSOR_CLASSIFICATION;

it('evaluates ultra precise thermometer', () => {
  const classification = thermometer.find(
    item => item.classification === 'ultra precise',
  );

  expect(
    classification.condition({ realValue: 70, std: 0.76, mean: 70.2 }),
  ).toBeTruthy();

  expect(
    classification.condition({ realValue: 70, std: 3, mean: 70.2 }),
  ).toBeFalsy();

  expect(
    classification.condition({ realValue: 70, std: 0.76, mean: 70.51 }),
  ).toBeFalsy();

  expect(classification.condition({ std: 0.76, mean: 70.2 })).toBeFalsy();

  expect(classification.condition({ realValue: 70, std: 0.76 })).toBeFalsy();

  expect(classification.condition({ realValue: 70, mean: 70.2 })).toBeFalsy();
});

it('evaluates very precise thermometer', () => {
  const classification = thermometer.find(
    item => item.classification === 'very precise',
  );

  expect(
    classification.condition({ realValue: 70, std: 4, mean: 70.2 }),
  ).toBeTruthy();

  expect(
    classification.condition({ realValue: 70, std: 5, mean: 70.2 }),
  ).toBeFalsy();

  expect(
    classification.condition({ realValue: 70, std: 0.76, mean: 70.51 }),
  ).toBeFalsy();

  expect(classification.condition({ std: 0.76, mean: 70.2 })).toBeFalsy();

  expect(classification.condition({ realValue: 70, std: 0.76 })).toBeFalsy();

  expect(classification.condition({ realValue: 70, mean: 70.2 })).toBeFalsy();
});

it('evaluates keep humidity sensor', () => {
  const classification = humidity.find(item => item.classification === 'keep');

  expect(classification.condition({ realValue: 50, value: 50.6 })).toBeTruthy();

  expect(classification.condition({ realValue: 50, value: 49 })).toBeTruthy();

  expect(classification.condition({ realValue: 50, value: 51 })).toBeTruthy();

  expect(classification.condition({ realValue: 70 })).toBeFalsy();

  expect(classification.condition({ value: 70 })).toBeFalsy();

  expect(classification.condition({ realValue: 50, value: 52 })).toBeFalsy();
});

it('evaluates keep monoxide sensor', () => {
  const classification = monoxide.find(item => item.classification === 'keep');

  expect(classification.condition({ realValue: 50, value: 50.6 })).toBeTruthy();

  expect(classification.condition({ realValue: 50, value: 47 })).toBeTruthy();

  expect(classification.condition({ realValue: 50, value: 53 })).toBeTruthy();

  expect(classification.condition({ realValue: 70 })).toBeFalsy();

  expect(classification.condition({ value: 70 })).toBeFalsy();

  expect(classification.condition({ realValue: 50, value: 56 })).toBeFalsy();
});
