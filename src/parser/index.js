'use strict';

import { PARSER } from '../config/parser';
import { Sensor } from '../sensor/index';
import moment from 'moment';

const {
  reference: {
    referenceIdentifier,
    referenceIdentifierIdx,
    sensors,
    separator: referenceSeparator,
  },
  sensor: { typeIdx, nameIdx, separator: sensorSeparator },
  reading: { dateTimeIdx, valueIdx, separator: readingSeparator },
} = PARSER;

export class Parser {
  constructor() {
    this.realValues = {};
    this.sensors = [];
  }

  evaluateRecord(record) {
    if (!record || record === '') {
      return;
    }

    if (this.checkAndSaveReferenceValues(record)) {
      return;
    }

    if (this.checkAndSaveCurrentSensor(record)) {
      return;
    }

    this.currentSensor && this.saveReading(record);
  }

  /* Parse record and checks if it the date is valid. In case it
   * is it and value is number it adds value to current sensor.
   */
  saveReading(record) {
    const splittedRecord = record.split(readingSeparator);
    const dateTime = splittedRecord[dateTimeIdx];

    if (!moment(dateTime).isValid()) {
      return;
    }

    const value = parseFloat(splittedRecord[valueIdx]);

    !isNaN(value) && this.currentSensor.addValue(value);
  }

  /* Parse record and checks if it is of sensor type. In case it
   * is it saves sensor as current sensor and adds it to array of
   * all sensors (but first checks if this sensor is not already
   * added there). Returns boolean based on result of type check.
   */
  checkAndSaveCurrentSensor(record) {
    const splittedRecord = record.split(sensorSeparator);
    const currentSensorType = splittedRecord[typeIdx];

    if (!sensors.includes(currentSensorType)) {
      return false;
    }

    const currentSensorName =
      splittedRecord[nameIdx] || `sensor-${this.sensors.length}`;

    // Checks if sensor is already saved.
    let currentSensor = this.sensors.find(
      sensor =>
        sensor.type === currentSensorType && sensor.name === currentSensorName,
    );

    // If sensor is not saved it create new one and adds it to array of all sensors.
    if (!currentSensor) {
      currentSensor = new Sensor(
        currentSensorType,
        currentSensorName,
        this.realValues[currentSensorType],
      );

      this.sensors.push(currentSensor);
    }

    this.currentSensor = currentSensor;

    return true;
  }

  /* Parse record and checks if it is of reference type. In case it
   * is it saves real values of all sensors. Returns boolean based
   * on result of type check.
   */
  checkAndSaveReferenceValues(record) {
    const splittedRecord = record.split(referenceSeparator);

    if (splittedRecord[referenceIdentifierIdx] !== referenceIdentifier) {
      return false;
    }

    let sensorIdx = 0;

    /* Save all sensor real values. It does not save reference identifier
     * index. All other sensors are saved based on order from config.
     */
    splittedRecord.forEach((item, idx) => {
      if (idx !== referenceIdentifierIdx) {
        this.saveSensor(item, sensorIdx);
        sensorIdx++;
      }
    });

    return true;
  }

  // Saves sensor's real value from reference record.
  saveSensor(value, idx) {
    const currentSensor = sensors[idx];

    if (currentSensor) {
      this.realValues[currentSensor] = parseFloat(value);
    }
  }
}
