import { Parser } from './index';

it('checks and saves reference value, returns true', () => {
  const parser = new Parser();

  const isReferenceRecord = parser.checkAndSaveReferenceValues(
    'reference 70.0 45.0 6',
  );

  expect(isReferenceRecord).toBeTruthy();

  expect(parser.realValues).toEqual({
    thermometer: 70,
    humidity: 45,
    monoxide: 6,
  });
});

it('checks reference value, retruns false', () => {
  const parser = new Parser();

  const isReferenceRecord = parser.checkAndSaveReferenceValues(
    'refeence 70.0 45.0 6',
  );

  expect(isReferenceRecord).toBeFalsy();

  expect(parser.realValues).toEqual({});
});

it('checks and saves current sensor, returns true', () => {
  const parser = new Parser();

  parser.checkAndSaveReferenceValues('reference 70.0 45.0 6');

  const isSensorRecord = parser.checkAndSaveCurrentSensor('thermometer temp-1');

  expect(isSensorRecord).toBeTruthy();
  expect(parser.currentSensor.name).toEqual('temp-1');
  expect(parser.currentSensor.type).toEqual('thermometer');
});

it('checks and saves current sensor with default name, returns true', () => {
  const parser = new Parser();

  parser.checkAndSaveReferenceValues('reference 70.0 45.0 6');

  const isSensorRecord = parser.checkAndSaveCurrentSensor('thermometer');

  expect(isSensorRecord).toBeTruthy();
  expect(parser.currentSensor.name).toEqual('sensor-0');
  expect(parser.currentSensor.type).toEqual('thermometer');
});

it('checks and picks current sensor from array od all sensors, returns true', () => {
  const parser = new Parser();

  parser.checkAndSaveReferenceValues('reference 70.0 45.0 6');

  parser.checkAndSaveCurrentSensor('thermometer temp-1');
  const isSensorRecord = parser.checkAndSaveCurrentSensor('thermometer temp-1');

  expect(isSensorRecord).toBeTruthy();
  expect(parser.sensors.length).toEqual(1);
});

it('checks current sensor, returns false', () => {
  const parser = new Parser();

  parser.checkAndSaveReferenceValues('reference 70.0 45.0 6');

  const isSensorRecord = parser.checkAndSaveCurrentSensor('termometer temp-1');

  expect(isSensorRecord).toBeFalsy();
});

it('saves readings', () => {
  const parser = new Parser();

  parser.checkAndSaveReferenceValues('reference 70.0 45.0 6');
  parser.checkAndSaveCurrentSensor('thermometer temp-1');
  parser.saveReading('2007-04-05T22:00 72.4');
  expect(parser.currentSensor.values).toEqual([72.4]);
});

it('does not save readings', () => {
  const parser = new Parser();

  parser.checkAndSaveReferenceValues('reference 70.0 45.0 6');
  parser.checkAndSaveCurrentSensor('thermometer temp-1');
  parser.saveReading('2007-13-05T22:00 72.4');
  parser.saveReading('2007-04-05T22:00 string');
  expect(parser.currentSensor.values).toEqual([]);
});

it('does not evaluate record', () => {
  const parser = new Parser();
  const jestFn = jest.fn();

  parser.checkAndSaveReferenceValues = jestFn;
  parser.checkAndSaveCurrentSensor = jestFn;
  parser.saveReading = jestFn;

  parser.evaluateRecord('');

  expect(jestFn.mock.calls.length).toBe(0);
});

it('evaluates reference record', () => {
  const parser = new Parser();
  const jestFn = jest
    .fn()
    .mockReturnValueOnce(true)
    .mockReturnValueOnce(false)
    .mockReturnValue();

  parser.checkAndSaveReferenceValues = jestFn;
  parser.checkAndSaveCurrentSensor = jestFn;
  parser.saveReading = jestFn;

  parser.evaluateRecord('record');

  expect(jestFn.mock.calls.length).toBe(1);
});

it('evaluates sensor record', () => {
  const parser = new Parser();
  const jestFn = jest
    .fn()
    .mockReturnValueOnce(false)
    .mockReturnValueOnce(true)
    .mockReturnValue();

  parser.checkAndSaveReferenceValues = jestFn;
  parser.checkAndSaveCurrentSensor = jestFn;
  parser.saveReading = jestFn;

  parser.evaluateRecord('record');

  expect(jestFn.mock.calls.length).toBe(2);
});

it('evaluates reading record', () => {
  const parser = new Parser();
  const jestFn = jest
    .fn()
    .mockReturnValueOnce(false)
    .mockReturnValueOnce(false)
    .mockReturnValue();

  parser.checkAndSaveReferenceValues = jestFn;
  parser.checkAndSaveCurrentSensor = jestFn;
  parser.saveReading = jestFn;
  parser.currentSensor = {};

  parser.evaluateRecord('record');

  expect(jestFn.mock.calls.length).toBe(3);
});

it('saves sensor and its real value', () => {
  const parser = new Parser();

  parser.saveSensor(70, 0);
  parser.saveSensor(50, 1);
  parser.saveSensor(55, 2);

  expect(parser.realValues).toEqual({
    thermometer: 70,
    humidity: 50,
    monoxide: 55,
  });
});
