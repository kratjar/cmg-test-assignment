'use strict';

import { PARSER } from './config/parser';
import { Parser } from './parser/index';

const {
  records: { separator: recordSeparator },
} = PARSER;

const evaluateLogFile = logContentsStr => {
  if (!logContentsStr || typeof logContentsStr !== 'string') {
    throw Error('Wrong input!');
  }

  const recordParser = new Parser();

  // Parse records from log string.
  const records = logContentsStr.split(recordSeparator);

  // Evaluate record using Parser class.
  records.forEach(line => {
    recordParser.evaluateRecord(line);
  });

  // Returns all sensors with their classification.
  return recordParser.sensors.map(item => ({
    [item.name]: item.getResult(),
  }));
};

export default evaluateLogFile;
