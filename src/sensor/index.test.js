import { Sensor } from './index';

const round = value => Math.round(value * 100) / 100;

const values = [69.6, 70.2, 69.75, 69.95, 70.15];

it('summarize array', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  expect(sensor.summarize([1, 2, null, 6])).toEqual(9);
  expect(sensor.summarize([1, 2, null, 6, undefined])).toEqual(NaN);
});

it('sums saved values', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  values.forEach(value => sensor.addValue(value));

  expect(sensor.sum()).toEqual(349.65);

  sensor.addValue(60);

  expect(sensor.sum()).toEqual(409.65);
});

it('returns total number of values', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  values.forEach(value => sensor.addValue(value));

  expect(sensor.total()).toEqual(5);

  sensor.addValue(60);

  expect(sensor.total()).toEqual(6);

  sensor.addValue(null);

  expect(sensor.total()).toEqual(7);
});

it('returns mean', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  values.forEach(value => sensor.addValue(value));

  expect(round(sensor.mean())).toEqual(69.93);

  sensor.addValue(60);

  expect(round(sensor.mean())).toEqual(68.27);

  sensor.addValue(null);

  expect(round(sensor.mean())).toEqual(58.52);
});

it('returns square diffs', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);
  const squareDiffs = [0.11, 0.07, 0.03, 0, 0.05];

  values.forEach(value => sensor.addValue(value));

  expect(sensor.squareDiffs().map(diff => round(diff))).toEqual(squareDiffs);
});

it('returns sum of square diffs', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  values.forEach(value => sensor.addValue(value));

  expect(round(sensor.sumOfSquareDiffs())).toEqual(0.26);
});

it('returns standard deviation', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  values.forEach(value => sensor.addValue(value));

  expect(round(sensor.std())).toEqual(0.26);
});

it('classifies thermometer as ultra precise', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  sensor.checkClassifications({
    mean: 70.2,
    std: 2,
    realValue: sensor.realValue,
  });

  expect(sensor.result).toEqual('ultra precise');
});

it('classifies thermometer as very precise', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  sensor.checkClassifications({
    mean: 70.2,
    std: 4,
    realValue: sensor.realValue,
  });

  expect(sensor.result).toEqual('very precise');
});

it('classifies thermometer as precise', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  sensor.checkClassifications({
    mean: 70.6,
    std: 4,
    realValue: sensor.realValue,
  });

  expect(sensor.result).toEqual('precise');

  sensor.checkClassifications({
    mean: 70.4,
    std: 6,
    realValue: sensor.realValue,
  });

  expect(sensor.result).toEqual('precise');
});

it('classifies humidity sensor as keep', () => {
  const sensor = new Sensor('humidity', 'hum-1', 50);

  sensor.checkClassifications({ value: 50.4, realValue: sensor.realValue });
  expect(sensor.result).toEqual('keep');

  sensor.checkClassifications({ value: 51, realValue: sensor.realValue });
  expect(sensor.result).toEqual('keep');

  sensor.checkClassifications({ value: 49, realValue: sensor.realValue });
  expect(sensor.result).toEqual('keep');
});

it('classifies humidity sensor as discard', () => {
  const sensor = new Sensor('humidity', 'hum-1', 50);

  sensor.checkClassifications({ value: 51.1, realValue: sensor.realValue });
  expect(sensor.result).toEqual('discard');

  sensor.checkClassifications({ value: 48.9, realValue: sensor.realValue });
  expect(sensor.result).toEqual('discard');
});

it('classifies monoxide sensor as keep', () => {
  const sensor = new Sensor('monoxide', 'mon-1', 50);

  sensor.checkClassifications({ value: 50.4, realValue: sensor.realValue });
  expect(sensor.result).toEqual('keep');

  sensor.checkClassifications({ value: 47, realValue: sensor.realValue });
  expect(sensor.result).toEqual('keep');

  sensor.checkClassifications({ value: 53, realValue: sensor.realValue });
  expect(sensor.result).toEqual('keep');
});

it('classifies monoxide sensor as discard', () => {
  const sensor = new Sensor('monoxide', 'hmon-1', 50);

  sensor.checkClassifications({ value: 53.1, realValue: sensor.realValue });
  expect(sensor.result).toEqual('discard');

  sensor.checkClassifications({ value: 46.9, realValue: sensor.realValue });
  expect(sensor.result).toEqual('discard');
});

it('calls checkClassifications method from classifyUsingMeanAndStd', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  sensor.checkClassifications = jest.fn();

  sensor.classifyUsingMeanAndStd();

  expect(sensor.checkClassifications).toBeCalled();
});

it('calls checkClassifications method from classifyUsingValueAndRealValue', () => {
  const sensor = new Sensor('humidity', 'hum-1', 50);

  sensor.checkClassifications = jest.fn();

  sensor.classifyUsingValueAndRealValue(50.5);

  expect(sensor.checkClassifications).toBeCalled();
});

it('calls classifyUsingMeanAndStd method from getResult in case of thermometer', () => {
  const sensor = new Sensor('thermometer', 'temp-1', 70);

  sensor.classifyUsingMeanAndStd = jest.fn();

  sensor.getResult();

  expect(sensor.classifyUsingMeanAndStd).toBeCalled();
});

it('calls classifyUsingValueAndRealValue method from addValue in case of non-thermometer sensor', () => {
  const sensor = new Sensor('humidity', 'hum-1', 50);

  sensor.classifyUsingValueAndRealValue = jest.fn();

  sensor.addValue(50.5);

  expect(sensor.classifyUsingValueAndRealValue).toBeCalled();
});
