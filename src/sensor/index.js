'use strict';

import {
  SENSOR_CLASSIFICATION,
  SENSORS_CLASSIFIED_USING_MEAN_AND_STD,
  SENSORS_CLASSIFIED_USING_VALUE_AND_REAL_VALUE,
} from '../config/sensors';

export class Sensor {
  constructor(type, name, realValue) {
    this.type = type;
    this.name = name;
    this.realValue = realValue;
    this.values = [];
    this.classifications = SENSOR_CLASSIFICATION[type]; // saves classifications from config based on current sensor type
  }

  // Summarizes read values.
  sum() {
    return this.summarize(this.values);
  }

  // Returns total number of read values.
  total() {
    return this.values.length;
  }

  // Calculates mean value of read values.
  mean() {
    return this.sum() / this.total();
  }

  // Creates array of square diffs of read values.
  squareDiffs() {
    return this.values.map(value => Math.pow(value - this.mean(), 2));
  }

  // Summarizes square diffs of read values.
  sumOfSquareDiffs() {
    return this.summarize(this.squareDiffs());
  }

  // Calculates standard deviation from sum of square diffs and total number of read values.
  std() {
    return Math.sqrt(this.sumOfSquareDiffs() / (this.total() - 1));
  }

  // Runs classification if needed and returns result of classification.
  getResult() {
    /* It classifies sensors which needs standard deviation and mean value after
     * all values are read (it needs all values to classify this type of sensor).
     */
    if (SENSORS_CLASSIFIED_USING_MEAN_AND_STD.includes(this.type)) {
      this.classifyUsingMeanAndStd();
    }

    return this.result;
  }

  // Adds value to array and runs classification if needed.
  addValue(value) {
    this.values.push(value);

    /* It classifies sensors which just compare real value to measured
     * value during line parsing (once classified as 'discard' it doesn't
     * need to run classification ever again).
     */
    if (
      SENSORS_CLASSIFIED_USING_VALUE_AND_REAL_VALUE.includes(this.type) &&
      this.result !== 'discard'
    ) {
      this.classifyUsingValueAndRealValue(value);
    }
  }

  // Classifies sensor using mean value and standard deviation.
  classifyUsingMeanAndStd() {
    const { realValue } = this;

    this.checkClassifications({
      realValue,
      std: this.std(),
      mean: this.mean(),
    });
  }

  // Classifies sensor using real value and provided value.
  classifyUsingValueAndRealValue(value) {
    const { realValue } = this;

    this.checkClassifications({ value, realValue });
  }

  // Goes through classifications from config and saves suited classification as result.
  checkClassifications(args) {
    for (let item of this.classifications) {
      const { condition, classification } = item;

      if ((condition && condition(args)) || !condition) {
        this.result = classification;
        break;
      }
    }
  }

  // Returns sum of array values.
  summarize(values) {
    return values.reduce((a, b) => a + b, 0);
  }
}
