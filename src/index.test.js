const evaluateLogFile = require('..');
const fs = require('fs');

const logContentsStr = fs.readFileSync('./data.log').toString();

it('evaluates log file', () => {
  expect(evaluateLogFile(logContentsStr)).toMatchSnapshot();
});
