# CMG - Test Assignment

[Task description](./CMG - Test Assignment.pdf)

## Instalation

```javascript
npm i https://gitlab.com/kratjar/cmg-test-assignment.git --s
```

## Import

```javascript
import evaluateLogFile from 'cmg-test-assignment';

or

const evaluateLogFile = require('cmg-test-assignment');
```
