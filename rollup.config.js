import pkg from './package.json';
import babel from 'rollup-plugin-babel';
import { terser } from 'rollup-plugin-terser';

const configuredBabel = () =>
  babel({
    exclude: ['node_modules/**'],
  });

export default [
  //UMD build
  {
    input: 'src/index.js',
    output: {
      name: 'evaluateLogFile',
      file: pkg.browser,
      format: 'umd',
      globals: {
        moment: 'moment',
      },
    },
    plugins: [configuredBabel(), terser()],
    external: ['moment'],
  },

  //CommonJS and ES module build
  {
    input: 'src/index.js',
    external: ['moment'],
    output: [
      { file: pkg.main, format: 'cjs' },
      { file: pkg.module, format: 'es' },
    ],
    plugins: [configuredBabel(), terser()],
  },
];
